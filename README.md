// Run the application
To the Spring Boot application, run the class BankAccountApplication in bank-account-war module in the package com.sg.kata.configuration

// Console Menu
When the application start, a menu will show up in the console.
A client with an empty account has been already created (in com.sg.kata.initializer.AppStartupRunner in the bank-account-war module)
You can make the following operation : 
    - Make a deposit statement;
    - Make a withdrawal statement;
    - See history;
    - See balance account;
    
When you finish testing, just click 0 to exit.


// Database
To access the database, just go to the link http://localhost:8080/h2-console

This is the database information access just in case :
    - url=jdbc:h2:mem:testdb
    - username=sa
    - password=
You can also find those information in the application.properties file in the server module.
It is in memory database. Everything will be gone when the application finish.





*******************************************************************
******************************* BDD *******************************
*******************************************************************

The project has 3  User Stories. We introduce a scenario for every user story :


**US1 : Make a deposit in a my account **

Feature : in order to save money 

As a bank client 

I want to make a deposit in my account 

Context : 

Given, I start the application with ruuning the BankAccountApplication class in the war module 

And then a menu appears. I choose (1) : make a deposit statement 

And I choose an amount 

**Scenario : make a desposit in my account for the first time **

Given 

I choose the amount that I want to deposit in my account 

When 

I click enter 

Then  

 

I get the amount and the new account balance 

 

**Scenario : Make a deposit in my account that already has money **

Given 

I choose the amount that I want to deposit in my account 

When 

I click enter 

Then  

 

I should see the final balance in my account which is the sum of my old balance with the amount I just deposited 

 

 

**US2 : Make a withdrawal from my account **

Feature : In order to retrieve some or all of my savings  

As a bank client 

I want to make a withdrawal from my account 

Context : 

Given, I start the application with ruuning the BankAccountApplication class in the war module 

And then a menu appears. I choose (2) : make a withdrawal statement 

And I choose an amount 

**Scénario : Withdraw all the money from my account **

Given 

I choose the amount that I want to withdraw from my account 

And 

The amount is equals to the account balance 

When 

I click enter 

Then  

 

I should see Operation success 

And 

The new balance should be equals to 0. 

 

**Scenario : Withdrawa some on my money **

Given 

I choose the amount that I want to withdraw from my account 

And 

The amount is less than the account balance 

When 

I click enter 

Then  

 

I should see Operation success 

And 

The remaining balance in my account is equal to the old balance minus the amount I withdrew. 

 

**Scénario : Withdraw a sum of money that is greater than the balance of my bank account **

Given 

I choose the amount that I want to withdraw from my account 

And 

The amount is greater than the account balance 

When 

I click enter 

Then  

 

The application should throw an exception 

And 

The account balance does not change 

 

**US3 : Check the account history **

Feature : In order to check my operations 

As a bank client 

I want to see the history (operation, date, amount, balance) of my operations 

Context : 

Given, I start the application with ruuning the BankAccountApplication class in the war module 

And then a menu appears. I choose (3) : See history  

**Scenario : View all the information about established transactions on my account **

Given 

I already have a bank account 

When 

I choose to see the operations history 

Then  

 

I should see the list of transactions I made : type, date, amount, new balance 

 
