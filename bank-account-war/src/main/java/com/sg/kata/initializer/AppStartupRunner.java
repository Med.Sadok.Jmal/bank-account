package com.sg.kata.initializer;

import com.sg.kata.Exception.InsufficientBalanceException;
import com.sg.kata.Exception.WrongAmountException;
import com.sg.kata.entity.Account;
import com.sg.kata.entity.AccountAudit;
import com.sg.kata.entity.Client;
import com.sg.kata.service.account.AccountService;
import com.sg.kata.service.account.strategy.DepositStrategy;
import com.sg.kata.service.account.strategy.WithdrawalStrategy;
import com.sg.kata.service.client.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

@Component
public class AppStartupRunner implements CommandLineRunner {

    @Autowired
    private ClientService clientService;

    @Autowired
    private AccountService accountService;

    private static Scanner scanner;

    //Constants
    private static final Logger LOG = LoggerFactory.getLogger(AppStartupRunner.class);

    @Override
    public void run(String...args) throws Exception {
        // Add client
        Client client = new Client("Alessandro", "Del Piero", "delpiero.alessandro@gmail.com");

        // Add account to the client
        Account account = new Account();
        account.setClient(client);
        accountService.save(account);


        // Menu
        LOG.info("***********************************************************************************************************");
        LOG.info("***** Hi there! We created a bank account for you!  *******************************************************");
        LOG.info("***** Unfortunately it is empty :( But no worries, you can make a deposit! Just try, it is for free :) ****");
        LOG.info("***********************************************************************************************************");

        int choice;
        do {
            choice = getOperationChoice();
            switch (choice) {
                case 0 : {
                    LOG.info("Bye bye!");
                    return;
                }
                case 1 : {
                    try {
                        accountService.makeNewStatement(new DepositStrategy(), account.getIdAccount(), getAmountChoice());
                    } catch (WrongAmountException | InsufficientBalanceException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 2 : {
                    try {
                        accountService.makeNewStatement(new WithdrawalStrategy(), account.getIdAccount(), getAmountChoice());
                    } catch (WrongAmountException | InsufficientBalanceException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 3 : {
                    List<AccountAudit> auditList = accountService.getAccountHistory(account.getIdAccount());
                    auditList.forEach(
                            accountAudit -> LOG.info(accountAudit.toString())
                    );
                    break;
                }
                case 4 : {
                    LOG.info("Current balance : " + accountService.getAccountById(account.getIdAccount()).getBalance());
                    break;
                }
            }
            typeAnythingContinue();
        } while (true);
    }

    /**
     * Choose Operation
     *
     * @return int
     */
    private int getOperationChoice() {
        scanner = new Scanner(System.in);
        int choice;
        do {
            try {
                LOG.info("Make a choice");
                LOG.info("0 - Exit");
                LOG.info("1 - Make a deposit statement");
                LOG.info("2 - Make a withdrawal statement");
                LOG.info("3 - See history");
                LOG.info("4 - See balance account");
                choice = scanner.nextInt();
            } catch (Exception e) {
                LOG.info("Wrong choice! Please choose a number between 0 and 4");
                scanner = new Scanner(System.in);
                choice = -999999;
            }
        } while (choice < 0 || choice > 4);
        return choice;
    }

    /**
     * Choose amount
     *
     * @return double
     */
    private double getAmountChoice() {
        scanner = new Scanner(System.in);
        double choice;
        do {
            try {
                LOG.info("Choose the statement amount");
                choice = scanner.nextDouble();
            } catch (Exception e) {
                LOG.info("Wrong choice! Please choose a double");
                scanner = new Scanner(System.in);
                choice = -999999;
            }
        } while (choice == -999999);
        return choice;
    }

    /**
     * Press any key to continue
     */
    private void typeAnythingContinue() {
        LOG.info("Type anything to continue...\n");
        try {
            System.in.read();
        } catch (Exception e) {}
    }
}
