package com.sg.kata.service;

import com.sg.kata.Exception.InsufficientBalanceException;
import com.sg.kata.Exception.WrongAmountException;
import com.sg.kata.entity.Account;

import com.sg.kata.entity.AccountAudit;
import com.sg.kata.repository.account.AccountRepository;
import com.sg.kata.repository.statement.StatementRepository;
import com.sg.kata.service.account.AccountAuditService;
import com.sg.kata.service.account.AccountAuditServiceImpl;
import com.sg.kata.service.account.AccountService;
import com.sg.kata.service.account.AccountServiceImpl;
import com.sg.kata.service.account.strategy.DepositStrategy;
import com.sg.kata.service.account.strategy.WithdrawalStrategy;
import com.sg.kata.service.statement.StatementService;
import com.sg.kata.service.statement.StatementServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class AccountServiceImplTest {

    @TestConfiguration
    static class AccountServiceImplTestContextConfiguration {

        @Bean
        public AccountService accountService() {
            return new AccountServiceImpl();
        }

        @Bean
        public AccountAuditService accountAuditService() {
            return new AccountAuditServiceImpl();
        }

        @Bean
        public StatementService statementService() {
            return new StatementServiceImpl();
        }
    }

    @Autowired
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private StatementRepository statementRepository;

    private Account account;

    private static final double AMOUNT = 200;
    private static final double INITIAL_AMOUNT = 100;
    private static final double NEGATIVE_AMOUNT = -50;
    private static final double TOLERANCE = 0;

    @Before
    public void init(){
        account = new Account(INITIAL_AMOUNT);
        when(accountRepository.save(account)).thenReturn(account);
        when(accountRepository.findById(account.getIdAccount())).thenReturn(Optional.of(account));
    }

    @Test
    public void accountServiceShouldDepositAmountWhenItIsGreaterThan0() {
        try {
            // when
            Account accountResult = accountService.makeNewStatement(new DepositStrategy(), account.getIdAccount(), AMOUNT);
            // then
            assertEquals(accountResult.getBalance(), INITIAL_AMOUNT + AMOUNT, TOLERANCE);
            assertEquals(account.getBalance(), INITIAL_AMOUNT + AMOUNT, TOLERANCE);
        } catch (WrongAmountException | InsufficientBalanceException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void accountServiceShouldThrowAnExceptionWhenAmountIsLessThan0() {
        try {
            // when
            accountService.makeNewStatement(new DepositStrategy(), account.getIdAccount(), NEGATIVE_AMOUNT);
            Assertions.fail("AccountService should throw a WrongAmountException");
        } catch (Exception exception) {
            assertSame(exception.getClass(), WrongAmountException.class);
        }
    }

    @Test
    public void accountServiceShouldThrowAnExceptionWhenWithdrawalAmountIsGreaterThanBalance() {
        try {
            // when
            accountService.makeNewStatement(new WithdrawalStrategy(), account.getIdAccount(), AMOUNT);
            Assertions.fail("AccountService should throw a InsufficientBalanceException");
        } catch (Exception exception) {
            assertSame(exception.getClass(), InsufficientBalanceException.class);
        }
    }

    @Test
    public void accountServiceShouldReturnAuditListWhenAccountExists() throws WrongAmountException, InsufficientBalanceException {
        // when
        accountService.makeNewStatement(new DepositStrategy(), account.getIdAccount(), AMOUNT);
        // then
        List<AccountAudit> auditResult = accountService.getAccountHistory(account.getIdAccount());
        assertNotNull(auditResult);
        assertEquals(auditResult.size(),1);
        assertEquals(auditResult.get(0).getAmount(), AMOUNT, TOLERANCE);
    }

}
