package com.sg.kata.repository.statement;

import com.sg.kata.entity.Operation;
import com.sg.kata.entity.Statement;
import com.sg.kata.repository.statement.StatementRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class StatementRepositoryTest {

    @Autowired
    private StatementRepository statementRepository;

    private Statement statement;

    private static final double TOLERANCE = 0;

    @Before
    public void init(){
        statement = new Statement(Operation.DEPOSIT, 200);
    }


    @Test
    public void statementRepositoryShouldSaveStatementWhenAllRequiredAttributesArePresent() {
        // when
        Statement statementResult = statementRepository.save(statement);

        // then
        assertNotNull(statementResult);
        assertEquals(statementResult.getAmount(), statement.getAmount(), TOLERANCE);
        assertEquals(statementResult.getDate(), statement.getDate());
        assertEquals(statementResult.getOperation(), statement.getOperation());
    }
}
