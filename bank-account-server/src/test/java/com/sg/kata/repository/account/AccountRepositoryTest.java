package com.sg.kata.repository.account;

import com.sg.kata.entity.Account;
import com.sg.kata.repository.account.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    private Account accountWithoutInitialAMount;
    private Account accountWithInitialAMount;

    private static final double INITIAL_AMOUNT = 200;
    private static final double DEFAULT_AMOUNT = 0;
    private static final double TOLERANCE = 0;

    @Before
    public void init(){
        accountWithoutInitialAMount = new Account();
        accountWithInitialAMount = new Account(INITIAL_AMOUNT);
    }


    @Test
    public void accountRepositoryShouldSaveAnEmptyAccountWhenNoInitialAmount() {
        // when
        Account accountResult = accountRepository.save(accountWithoutInitialAMount);

        // then
        assertNotNull(accountResult);
        assertEquals(accountResult.getBalance(), DEFAULT_AMOUNT, TOLERANCE);
    }

    @Test
    public void accountRepositoryShouldSaveAccountWithInitialAmount() {
        // when
        Account accountResult = accountRepository.save(accountWithInitialAMount);

        // then
        assertNotNull(accountResult);
        assertEquals(accountResult.getBalance(), INITIAL_AMOUNT, TOLERANCE);
    }
}
