package com.sg.kata.repository.client;

import com.sg.kata.entity.Client;
import com.sg.kata.repository.client.ClientRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class ClientRepositoryTest {

    @Autowired
    private ClientRepository clientRepository;

    private Client client;

    @Before
    public void init(){
        client = new Client("Pavel", "Nedved", "nedved.pavel@gmail.com");
    }


    @Test
    public void clientRepositoryShouldSaveClientWhenAllRequiredAttributesArePresent() {
        // when
        Client clientResult = clientRepository.save(client);

        // then
        assertNotNull(clientResult);
        assertEquals(clientResult.getFirstName(), client.getFirstName());
        assertEquals(clientResult.getLastName(), client.getLastName());
        assertEquals(clientResult.getEmail(), client.getEmail());
    }
}
