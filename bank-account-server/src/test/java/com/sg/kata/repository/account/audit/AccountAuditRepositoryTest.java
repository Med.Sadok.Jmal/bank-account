package com.sg.kata.repository.account.audit;

import com.sg.kata.entity.Account;
import com.sg.kata.entity.AccountAudit;
import com.sg.kata.entity.Operation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class AccountAuditRepositoryTest {

    @Autowired
    private AccountAuditRepository accountAuditRepository;

    private AccountAudit accountAudit;

    @Mock
    private Account account;

    private static final double TOLERANCE = 0;

    @Before
    public void init(){
        accountAudit = new AccountAudit(Operation.DEPOSIT, LocalDateTime.now(), 200, 230, account);
    }


    @Test
    public void accountAuditRepositoryShouldSaveAuditWhenAllRequiredAttributesArePresent() {
        // when
        AccountAudit auditResult = accountAuditRepository.save(accountAudit);

        // then
        assertNotNull(auditResult);
        assertEquals(auditResult.getOperation(), accountAudit.getOperation());
        assertEquals(auditResult.getDate(), accountAudit.getDate());
        assertEquals(auditResult.getAmount(), accountAudit.getAmount(), TOLERANCE);
        assertEquals(auditResult.getBalance(), accountAudit.getBalance(), TOLERANCE);
    }
}
