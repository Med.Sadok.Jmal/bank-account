package com.sg.kata.service.statement;

import com.sg.kata.entity.Statement;

import java.util.Set;

public interface StatementService {

    Statement save(Statement statement);
    Set<Statement> getAccountStatements(long idAccount);
}
