package com.sg.kata.service.statement;

import com.sg.kata.entity.Statement;
import com.sg.kata.repository.statement.StatementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@Transactional
public class StatementServiceImpl implements StatementService {

    @Autowired
    private StatementRepository statementRepository;

    //Constants
    private static final Logger LOG = LoggerFactory.getLogger(StatementRepository.class);

    // Methods

    /**
     * Adding or updating Statement
     *
     * @param statement
     * @return Statement
     */
    @Override
    public Statement save(Statement statement) {
        LOG.info("About to save client");
        Statement statementResult = null;
        try {
            statementResult = statementRepository.save(statement);
        } catch (Exception exception) {
            LOG.error("Error while saving statement");
            exception.getStackTrace();
        }
        return statementResult;
    }

    /**
     * Get all account statements
     *
     * @param idAccount
     * @return
     */
    @Override
    public Set<Statement> getAccountStatements(long idAccount) {
        return statementRepository.getAccountStatements(idAccount);
    }
}
