package com.sg.kata.service.account;

import com.sg.kata.Exception.InsufficientBalanceException;
import com.sg.kata.Exception.WrongAmountException;
import com.sg.kata.entity.Account;
import com.sg.kata.entity.AccountAudit;
import com.sg.kata.service.account.strategy.StatementStrategy;

import java.util.List;

public interface AccountService {

    Account save(Account account);
    Account makeNewStatement(StatementStrategy statementStrategy, long idAccount, double amount) throws WrongAmountException, InsufficientBalanceException;
    List<AccountAudit> getAccountHistory(long idAccount);
    Account getAccountById(long idAccount);
}
