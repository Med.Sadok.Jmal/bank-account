package com.sg.kata.service.account;

import com.sg.kata.entity.AccountAudit;

import java.util.List;

public interface AccountAuditService {

    AccountAudit save(AccountAudit accountAudit);
    List<AccountAudit> getAccountHistory(long idAccount);
}
