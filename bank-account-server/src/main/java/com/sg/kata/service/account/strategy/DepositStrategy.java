package com.sg.kata.service.account.strategy;

import com.sg.kata.entity.Account;
import com.sg.kata.entity.Operation;
import com.sg.kata.entity.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepositStrategy implements StatementStrategy {

    //Constants
    private static final Logger LOG = LoggerFactory.getLogger(DepositStrategy.class);

    @Override
    public void checkBalance(Account account, double amount) {

    }

    @Override
    public Statement prepareStatement(Account account, double amount) {
        LOG.info("About to deposit " + amount + " to the account " + account.getIdAccount());
        Statement statement = new Statement(Operation.DEPOSIT, amount);
        account.addDepositStatement(statement);
        statement.setAccount(account);
        return statement;
    }

    @Override
    public void finish(Account account, double amount) {
        LOG.info("The deposit of " + amount + " has been successfully made in the account " + account.getIdAccount());
        LOG.info("New balance : " + account.getBalance());
    }
}
