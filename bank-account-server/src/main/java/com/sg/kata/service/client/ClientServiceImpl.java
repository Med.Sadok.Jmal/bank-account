package com.sg.kata.service.client;

import com.sg.kata.entity.Client;
import com.sg.kata.repository.client.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    //Constants
    private static final Logger LOG = LoggerFactory.getLogger(ClientServiceImpl.class);

    // Methods

    /**
     * Adding or updating Client
     *
     * @param client
     * @return Client
     */
    @Override
    public Client save(Client client) {
        LOG.info("About to save client");
        Client clientResult = null;
        try {
            clientResult = clientRepository.save(client);
        } catch (Exception exception) {
            LOG.error("Error while saving client");
            exception.getStackTrace();
        }
        LOG.info("The client was successfully saved");
        return clientResult;
    }
}
