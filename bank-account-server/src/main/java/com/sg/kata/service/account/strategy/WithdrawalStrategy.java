package com.sg.kata.service.account.strategy;

import com.sg.kata.Exception.ExceptionCode;
import com.sg.kata.Exception.InsufficientBalanceException;
import com.sg.kata.entity.Account;
import com.sg.kata.entity.Operation;
import com.sg.kata.entity.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WithdrawalStrategy implements StatementStrategy {

    //Constants
    private static final Logger LOG = LoggerFactory.getLogger(WithdrawalStrategy.class);

    @Override
    public void checkBalance(Account account, double amount) throws InsufficientBalanceException {
        if (amount > account.getBalance()) {
            throw new InsufficientBalanceException(this.getClass(), ExceptionCode.INSUFFICIENT_BALANCE);
        }
    }

    @Override
    public Statement prepareStatement(Account account, double amount) {
        LOG.info("About to withdraw " + amount + " from the account " + account.getIdAccount());
        Statement statement = new Statement(Operation.WITHDRAWAL, amount);
        account.addWithdrawalStatement(statement);
        statement.setAccount(account);
        return statement;
    }

    @Override
    public void finish(Account account, double amount) {
        LOG.info("The withdrawal of " + amount + " has been successfully made from the account " + account.getIdAccount());
        LOG.info("New balance : " + account.getBalance());
    }
}
