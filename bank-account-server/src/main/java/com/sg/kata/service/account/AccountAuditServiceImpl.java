package com.sg.kata.service.account;

import com.sg.kata.entity.AccountAudit;
import com.sg.kata.repository.account.audit.AccountAuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountAuditServiceImpl implements AccountAuditService {

    @Autowired
    private AccountAuditRepository accountAuditRepository;

    //Constants
    private static final Logger LOG = LoggerFactory.getLogger(AccountAuditServiceImpl.class);

    // Methods

    /**
     * Adding or updating AccountAudit
     *
     * @param accountAudit
     * @return AccountAudit
     */
    @Override
    public AccountAudit save(AccountAudit accountAudit) {
        LOG.info("About to save account audit");
        AccountAudit accountAuditResult = null;
        try {
            accountAuditResult = accountAuditRepository.save(accountAudit);
        } catch (Exception exception) {
            LOG.error("Error while saving account audit");
            exception.getStackTrace();
        }
        LOG.info("The account audit was successfully saved");
        return accountAuditResult;
    }

    /**
     * Get Account history
     *
     * @param idAccount
     * @return Set<AccountAudit>
     */
    @Override
    public List<AccountAudit> getAccountHistory(long idAccount) {
        LOG.info("Search history for the account " + idAccount);
        return accountAuditRepository.getAuditByAccountId(idAccount);
    }
}
