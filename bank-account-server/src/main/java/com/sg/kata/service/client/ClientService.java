package com.sg.kata.service.client;

import com.sg.kata.entity.Client;

public interface ClientService {

    Client save(Client client);
}
