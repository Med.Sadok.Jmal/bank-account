package com.sg.kata.service.account.strategy;

import com.sg.kata.Exception.InsufficientBalanceException;
import com.sg.kata.entity.Account;
import com.sg.kata.entity.Statement;

public interface StatementStrategy {
    
    void checkBalance(Account account, double amount) throws InsufficientBalanceException;

    Statement prepareStatement(Account account, double amount);

    void finish(Account account, double amount);
}
