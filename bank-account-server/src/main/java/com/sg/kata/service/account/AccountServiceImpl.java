package com.sg.kata.service.account;

import com.sg.kata.Exception.WrongAmountException;
import com.sg.kata.Exception.ExceptionCode;
import com.sg.kata.Exception.InsufficientBalanceException;
import com.sg.kata.entity.Account;
import com.sg.kata.entity.AccountAudit;
import com.sg.kata.entity.Statement;
import com.sg.kata.repository.account.AccountRepository;
import com.sg.kata.service.statement.StatementService;
import com.sg.kata.service.account.strategy.StatementStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private StatementService statementService;

    @Autowired
    private AccountAuditService accountAuditService;

    @PersistenceContext
    private EntityManager entityManager;

    //Constants
    private static final Logger LOG = LoggerFactory.getLogger(AccountServiceImpl.class);

    // Methods

    /**
     * Adding or updating Account
     *
     * @param account
     * @return Account
     */
    @Override
    public Account save(Account account) {
        LOG.info("About to save account");
        Account accountResult = null;
        try {
            accountResult = accountRepository.save(account);
        } catch (Exception exception) {
            LOG.error("Error while saving account");
            exception.getStackTrace();
        }
        LOG.info("The account was successfully saved");
        return accountResult;
    }

    /**
     * Make a deposit or a withdrawal
     *
     * @param statementStrategy
     * @param idAccount
     * @param amount
     * @return account
     * @throws WrongAmountException
     * @throws InsufficientBalanceException
     */
    @Override
    public Account makeNewStatement(StatementStrategy statementStrategy, long idAccount, double amount) throws WrongAmountException, InsufficientBalanceException {
        Account account = accountRepository.findById(idAccount).orElse(null);

        if (account == null) {
            LOG.error("Account cannot be null");
            return null;
        }
        if (amount <= 0) {
            throw new WrongAmountException(this.getClass(), ExceptionCode.DEPOSIT_AMOUNT);
        }

        // Check if the balance is sufficient
        statementStrategy.checkBalance(account, amount);

        // create a new deposit or withdrawal statement
        account.setStatements(statementService.getAccountStatements(idAccount));
        Statement statement = statementStrategy.prepareStatement(account, amount);

        // Save in the DB
        try {
            //entityManager.merge(account);
            statementService.save(statement);
        } catch (Exception exception) {
            LOG.error("Error while trying to add new statement to the account" + account.getIdAccount());
            exception.getStackTrace();
        }
        statementStrategy.finish(account, amount);

        // Audit
        AccountAudit accountAudit = new AccountAudit(
                statement.getOperation(),
                statement.getDate(),
                statement.getAmount(),
                account.getBalance(),
                account
        );
        try {
            accountAuditService.save(accountAudit);
        } catch (Exception exception) {
            LOG.error("Error while trying to add new account audit to the account" + account.getIdAccount());
            exception.getStackTrace();
        }
        LOG.info("Statement Audit was successfully added to account : " + account.getIdAccount());

        return account;
    }

    /**
     * Get Account Statement History
     *
     * @param idAccount
     * @return
     */
    @Override
    public List<AccountAudit> getAccountHistory(long idAccount) {
        return accountAuditService.getAccountHistory(idAccount);
    }

    /**
     * Get account by its Id
     * @param idAccount
     * @return
     */
    @Override
    public Account getAccountById(long idAccount) {
        return accountRepository.findById(idAccount).orElse(null);
    }
}
