package com.sg.kata.Exception;

public enum ExceptionCode {

    DEPOSIT_AMOUNT("The amount should be greater than 0"),
    INSUFFICIENT_BALANCE("To make a withdrawal from an account, the amount should be less than the balance");

    private final String message;

    ExceptionCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
