package com.sg.kata.Exception;

public class InsufficientBalanceException extends BankAccountException {

    public InsufficientBalanceException(Class clazz, ExceptionCode code) {
        super(clazz, code);
    }
}
