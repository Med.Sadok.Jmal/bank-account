package com.sg.kata.Exception;

public class BankAccountException extends Exception {

    private String message;

    public BankAccountException(Class clazz, ExceptionCode code) {
        this.message = "[" + clazz.getSimpleName() + "]  : " + code.getMessage();
    }

    @Override
    public String getMessage() {
        return message;
    }
}
