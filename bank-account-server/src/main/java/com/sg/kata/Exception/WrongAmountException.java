package com.sg.kata.Exception;

public class WrongAmountException extends BankAccountException {

    public WrongAmountException(Class clazz, ExceptionCode code) {
        super(clazz, code);
    }
}
