package com.sg.kata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.sg.kata")
public class BankAccountServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAccountServerApplication.class, args);
    }
}
