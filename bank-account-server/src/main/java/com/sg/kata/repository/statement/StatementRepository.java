package com.sg.kata.repository.statement;


import com.sg.kata.entity.Statement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatementRepository extends CrudRepository<Statement, Long>, StatementCustomisedRepository {
}
