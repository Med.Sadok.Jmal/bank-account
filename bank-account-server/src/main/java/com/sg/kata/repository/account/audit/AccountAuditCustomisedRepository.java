package com.sg.kata.repository.account.audit;

import com.sg.kata.entity.AccountAudit;

import java.util.List;

public interface AccountAuditCustomisedRepository {

    List<AccountAudit> getAuditByAccountId(long idAccount);
}
