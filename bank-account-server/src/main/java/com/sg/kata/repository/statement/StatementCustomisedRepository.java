package com.sg.kata.repository.statement;

import com.sg.kata.entity.Statement;

import java.util.Set;

public interface StatementCustomisedRepository {

    Set<Statement> getAccountStatements(long idAccount);
}
