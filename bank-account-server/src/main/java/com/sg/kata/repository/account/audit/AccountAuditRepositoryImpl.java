package com.sg.kata.repository.account.audit;

import com.sg.kata.entity.AccountAudit;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class AccountAuditRepositoryImpl implements AccountAuditCustomisedRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<AccountAudit> getAuditByAccountId(long idAccount) {
        Query query = entityManager.createNativeQuery(
                "SELECT audit.* FROM account_audit audit WHERE audit.id_account = :idAccount",
                AccountAudit.class
        );
        query.setParameter("idAccount", idAccount);
        return Optional.ofNullable(query.getResultList()).orElse(new ArrayList<>());
    }
}
