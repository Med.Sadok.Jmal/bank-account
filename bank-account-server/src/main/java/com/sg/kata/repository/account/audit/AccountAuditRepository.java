package com.sg.kata.repository.account.audit;

import com.sg.kata.entity.AccountAudit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountAuditRepository extends CrudRepository<AccountAudit, Long>, AccountAuditCustomisedRepository {

}
