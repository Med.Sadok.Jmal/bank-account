package com.sg.kata.repository.statement;

import com.sg.kata.entity.Statement;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Repository
public class StatementRepositoryImpl implements StatementCustomisedRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Set<Statement> getAccountStatements(long idAccount) {
        Query query = entityManager.createNativeQuery(
                "SELECT * FROM statement WHERE id_account = :idAccount",
                Statement.class
        );
        query.setParameter("idAccount", idAccount);
        List<Statement> listResult = Optional.ofNullable(query.getResultList()).orElse(new ArrayList());
        return new HashSet<>(listResult);
    }
}
