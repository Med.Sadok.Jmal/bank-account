package com.sg.kata.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "account")
public class Account {

    //Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ACCOUNT")
    private long idAccount;

    @Column(name = "BALANCE")
    @NotNull(message = "Account balance is required")
    private double balance;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private Set<Statement> statements;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<AccountAudit> accountAudits;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CLIENT")
    private Client client;

    //Constants
    private static final double INITIAL_BALANCE = 0;

    // Constructors
    public Account() {
        balance = INITIAL_BALANCE;
        statements = new HashSet<>();
        accountAudits = new ArrayList<>();
    }

    public Account(double balance) {
        this.balance = balance;
        statements = new HashSet<>();
        accountAudits = new ArrayList<>();
    }

    // Getters and Setters
    public long getIdAccount() {
        return idAccount;
    }

    public double getBalance() {
        return balance;
    }

    public Set<Statement> getStatements() {
        // unmodifiable set to disable adding statement without updating balance
        return Collections.unmodifiableSet(statements);
    }

    public void setStatements(Set<Statement> statements) {
        this.statements = statements;
    }

    public void addDepositStatement(Statement statement) {
        this.statements.add(statement);
        this.balance += statement.getAmount();
    }

    public void addWithdrawalStatement(Statement statement) {
        this.statements.add(statement);
        this.balance -= statement.getAmount();
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


    public List<AccountAudit> getAccountAudits() {
        return accountAudits;
    }

    public void setAccountAudits(List<AccountAudit> accountAudits) {
        this.accountAudits = accountAudits;
    }
}

