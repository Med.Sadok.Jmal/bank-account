package com.sg.kata.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "account_audit")
public class AccountAudit {

    //Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ACCOUNT_AUDIT")
    private long idAccountAudit;

    @Column(name = "OPERATION")
    @NotNull(message = "Account Audit operation is required")
    @Enumerated(EnumType.STRING)
    private Operation operation;

    @Column(name = "DATE")
    private LocalDateTime date;

    @Column(name = "AMOUNT")
    @NotNull(message = "Account Audit amount is required")
    private double amount;

    @Column(name = "BALANCE")
    private double balance;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ACCOUNT")
    private Account account;

    //Constructors
    private AccountAudit() {}

    public AccountAudit(Operation operation, LocalDateTime date, double amount, double balance, Account account) {
        this.operation = operation;
        this.date = date;
        this.amount = amount;
        this.balance = balance;
        this.account = account;
    }

    // Methods
    @Override
    public String toString() {
        String formattedDate = "";
        if (date != null) {
            formattedDate = ", date : " + date.format(DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm:ss"));
        }
        return operation + " " + amount + formattedDate + ", new balance = " + balance;
    }

    //Getters and Setters
    public long getIdAccountAudit() {
        return idAccountAudit;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
