package com.sg.kata.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "statement")
public class Statement {

    //Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_STATEMENT")
    private long idStatement;

    @Column(name = "OPERATION")
    @NotNull(message = "Statement operation is required")
    @Enumerated(EnumType.STRING)
    private Operation operation;

    @Column(name = "AMOUNT")
    @NotNull(message = "Statement amount is required")
    private double amount;

    @Column(name = "DATE")
    private LocalDateTime date;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ACCOUNT")
    private Account account;

    //Constructors
    private Statement() {}

    public Statement(Operation operation, double amount) {
        this.operation = operation;
        this.amount = amount;
        this.date = LocalDateTime.now();
    }

    // Override equals and hashcode to handle Set<Statement> in Account
    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (!(object instanceof Statement)) return false;
        return idStatement == ((Statement) object).getIdStatement();
    }

    @Override
    public int hashCode() {
        return (int) idStatement;
    }

    //Getters and Setters
    public long getIdStatement() {
        return idStatement;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}