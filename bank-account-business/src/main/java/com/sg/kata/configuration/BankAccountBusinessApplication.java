package com.sg.kata.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankAccountBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAccountBusinessApplication.class, args);
    }

}